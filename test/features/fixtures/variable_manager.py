import os
import re

from behave import fixture
from dotenv import load_dotenv


class VariableManager:
    def __init__(self):
        self.variables = {}
        load_dotenv()

    def set(self, variable_name, value):
        self.variables[variable_name] = value

    def get(self, variable_name) -> str:
        return str(self.variables[variable_name])

    def exists(self, variable_name):
        return variable_name in self.variables

    def get_replaced_value(self, text: str):
        # Replace session values
        matchs = re.findall(r"{session:(.*?)}", text)
        for match in matchs:
            assert self.exists(match), f'Session variable {match} not exists'
            text = text.replace('{session:' + match + '}', self.get(match))
        # Replace environment values
        matchs = re.findall(r"{env:(.*?)}", text)
        for match in matchs:
            assert os.getenv(match) is not None, f'Environment variable {match} not exists'
            text = text.replace('{env:' + match + '}', os.getenv(match))
        return text


@fixture
def before_all(context):
    context.variables_manager = VariableManager()
