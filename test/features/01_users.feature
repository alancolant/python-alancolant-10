Feature: API - Users

  Background:
    Given set base_url "{env:API_URL}"


  Scenario: Signup return errors when incorrect data
    #Action
    When do "POST" request to "/api/signup"

    Then response code is "400"
    And response has JSON structure
      | Field        | Value |
      | username.0   | N/A   |
      | password.0   | N/A   |
      | password2.0  | N/A   |
      | email.0      | N/A   |
      | first_name.0 | N/A   |
      | last_name.0  | N/A   |

  @depend('create')
  Scenario: Can create user
    When do "POST" request to "/api/signup"
      | Field      | Value              |
      | username   | {env:API_USERNAME} |
      | password   | {env:API_PASSWORD} |
      | password2  | {env:API_PASSWORD} |
      | email      | tester@example.com |
      | first_name | Tester             |
      | last_name  | Example            |

    Then response code is "201"
    And response has JSON structure
      | Field      | Value              |
      | username   | {env:API_USERNAME} |
      | email      | tester@example.com |
      | first_name | Tester             |
      | last_name  | Example            |

  @dependsOnPassed('create')
  Scenario: Can login user
    #Action
    When do "POST" request to "/api/login"
      | Field    | Value              |
      | username | {env:API_USERNAME} |
      | password | {env:API_PASSWORD} |

    Then response code is "200"
    And response has JSON structure
      | Field   | Value |
      | access  | N/A   |
      | refresh | N/A   |
