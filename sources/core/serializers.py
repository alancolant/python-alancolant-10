from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator

from core.models import Project, Contributor, Issue, Comment


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'password2', 'email', 'first_name', 'last_name')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email']


class ContributorSerializer(ModelSerializer):
    user = UserSerializer(many=False, read_only=True)
    user_id = serializers.ModelField(User()._meta.get_field('id'), write_only=True)

    role = serializers.ModelField(Contributor()._meta.get_field('role'), read_only=True)

    def validate_user_id(self, value):
        """Ensure user is not already assigned to project, and it's not the author"""
        query = Contributor.objects.filter(
            user_id=value,
            project_id=self.context['request'].parser_context['kwargs']['project_pk']
        )

        if self.context['view'].action in ("update", "partial_update", "destroy"):
            # Cannot update author
            contributor = Contributor.objects.filter(id=self.context['request'].parser_context['kwargs']['pk']).get()
            if contributor.role == 'author':
                raise serializers.ValidationError("You cannot update author")
            query = query.exclude(id=self.context['request'].parser_context['kwargs']['pk'])

        if query.exists():
            raise serializers.ValidationError("This user is already a contributor")

        return value

    def validate(self, attrs):
        attrs['project_id'] = self.context['request'].parser_context['kwargs']['project_pk']
        attrs['role'] = "contributor"
        print(attrs)
        return super().validate(attrs)

    class Meta:
        model = Contributor
        fields = ['id', 'user', 'user_id', 'role']


class CommentSerializer(ModelSerializer):
    author = UserSerializer(many=False, read_only=True)

    def validate(self, attrs):
        attrs['issue_id'] = self.context['request'].parser_context['kwargs']['issue_pk']
        attrs['author_id'] = self.context['request'].user.id
        return super().validate(attrs)

    class Meta:
        model = Comment
        fields = ['id', 'description', 'author', 'created_time']


class IssueSerializer(ModelSerializer):
    author = UserSerializer(many=False, read_only=True)

    assignee = UserSerializer(many=False, read_only=True)
    assignee_id = serializers.ModelField(User()._meta.get_field('id'), write_only=True, required=False)

    comments = CommentSerializer(many=True, read_only=True)

    def validate(self, attrs):
        attrs['project_id'] = self.context['request'].parser_context['kwargs']['project_pk']
        attrs['author'] = self.context['request'].user
        return super().validate(attrs)

    class Meta:
        model = Issue
        fields = ['id', 'title', 'desc', 'tag', 'priority', 'status', 'author', 'assignee', 'assignee_id',
                  'created_time', 'comments']


class ProjectSerializer(ModelSerializer):
    issues = IssueSerializer(many=True, read_only=True)
    contributors = ContributorSerializer(many=True, read_only=True)

    def create(self, validated_data):
        """Automatically create contributor entry"""
        created: Project = super().create(validated_data)
        Contributor.objects.create(user=self.context['request'].user, project=created, role="author")
        return created

    class Meta:
        model = Project
        fields = ['id', 'title', 'description', 'type', 'contributors', 'issues']
