from rest_framework.permissions import BasePermission
from rest_framework.request import Request

from core.models import Comment, Issue, Contributor


class IsProjectContributorPermission(BasePermission):
    def __init__(self, project_id):
        super().__init__()
        self.project_id = project_id

    def __call__(self, *args, **kwargs):
        return self

    def has_permission(self, request: Request, view):
        return Contributor.objects.filter(
            project=self.project_id,
            user=request.user
        ).exists()


class IsProjectAuthorPermission(BasePermission):
    def __init__(self, project_id):
        super().__init__()
        self.project_id = project_id

    def __call__(self, *args, **kwargs):
        return self

    def has_permission(self, request: Request, view):
        return Contributor.objects.filter(
            project=self.project_id,
            user=request.user,
            role='author'
        ).exists()


class IsIssueAuthorPermission(BasePermission):
    def __init__(self, issue_id):
        super().__init__()
        self.issue_id = issue_id

    def __call__(self, *args, **kwargs):
        return self

    def has_permission(self, request: Request, view):
        return Issue.objects.filter(
            pk=self.issue_id,
            author=request.user,
        ).exists()


class IsCommentAuthorPermission(BasePermission):
    def __init__(self, comment_id):
        super().__init__()
        self.comment_id = comment_id

    def __call__(self, *args, **kwargs):
        return self

    def has_permission(self, request: Request, view):
        return Comment.objects.filter(
            pk=self.comment_id,
            author=request.user,
        ).exists()
