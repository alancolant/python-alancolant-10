Feature: API - Issues - Errors

  Background:
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "otherUser" with password "randomPasswordToOtherUser"

    #Create new project project
    When do "POST" request to "/api/projects/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |

    Then response code is "201"
    And add JSON value "id" in session as "project_id"

    Given ensure bearer_token is set for "{env:API_USERNAME}" with password "{env:API_PASSWORD}"


  Scenario: User cannot create issue in other project
    When do "POST" request to "/api/projects/{session:project_id}/issues/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |
    Then response code is "403"

  Scenario: User cannot list issues in other projects
    When do "GET" request to "/api/projects/{session:project_id}/issues"
    Then response code is "403"