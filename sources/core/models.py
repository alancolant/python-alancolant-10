from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Project(models.Model):
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=8192)
    type = models.CharField(
        max_length=9,
        choices=[(c, _(c)) for c in ["back-end", "front-end", "iOS", "Android"]]
    )

    def __str__(self):
        return self.title

    class Meta:
        db_table = "projects"


class Contributor(models.Model):
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="contributors")
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="contributors")
    # permission = models //@TODO
    role = models.CharField(max_length=64)

    class Meta:
        db_table = "contributors"
        constraints = [models.UniqueConstraint(fields=['user', 'project'], name="contributors_user_project_unique")]


class Issue(models.Model):
    title = models.CharField(max_length=128)
    desc = models.CharField(max_length=8192)
    tag = models.CharField(
        max_length=12,

        choices=[(c, _(c)) for c in ["bug", "amelioration", "task"]]
    )
    priority = models.CharField(
        max_length=6,
        choices=[(c, _(c)) for c in ["low", "medium", "high"]]
    )
    status = models.CharField(
        max_length=6,
        choices=[(c, _(c)) for c in ["todo", "wip", "finish"]]
    )
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="issues")

    author = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="issues")
    assignee = models.ForeignKey(
        to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="assigned_issues",
        blank=True, null=True)

    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "issues"


class Comment(models.Model):
    description = models.CharField(max_length=8192)
    author = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="comments")
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name="comments")

    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description

    class Meta:
        db_table = "comments"
