from django.contrib.auth.models import User
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet

from core.models import Project, Issue, Comment, Contributor
from core.permissions import IsProjectAuthorPermission, IsProjectContributorPermission, IsIssueAuthorPermission, \
    IsCommentAuthorPermission
from core.serializers import ProjectSerializer, IssueSerializer, CommentSerializer, RegisterSerializer, \
    ContributorSerializer


# User registering
class RegisterView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class ProjectViewSet(ModelViewSet):
    serializer_class = ProjectSerializer

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated]
        if self.action in ('destroy', 'update', 'partial_update'):
            self.permission_classes.append(IsProjectAuthorPermission(self.kwargs['pk']))
        elif self.action == 'retrieve':
            self.permission_classes.append(IsProjectContributorPermission(self.kwargs['pk']))

        return super(self.__class__, self).get_permissions()

    def get_queryset(self):
        return Project.objects.filter(contributors__user=self.request.user).prefetch_related('contributors').all()


class ContributorViewSet(ModelViewSet):
    serializer_class = ContributorSerializer

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated]
        if self.action in ('destroy', 'update', 'partial_update', 'create'):
            self.permission_classes.append(IsProjectAuthorPermission(self.kwargs['project_pk']))
        elif self.action in ('retrieve', 'list'):
            self.permission_classes.append(IsProjectContributorPermission(self.kwargs['project_pk']))

        return super(self.__class__, self).get_permissions()

    def get_queryset(self):
        return Contributor.objects.filter(project=self.kwargs['project_pk']).all()


class IssueViewSet(ModelViewSet):
    serializer_class = IssueSerializer

    def get_permissions(self):
        self.permission_classes = [
            IsAuthenticated,
            IsProjectContributorPermission(self.kwargs['project_pk'])
        ]

        if self.action in ('destroy', 'update', 'partial_update'):
            self.permission_classes.append(IsIssueAuthorPermission(self.kwargs['pk']))

        return super(self.__class__, self).get_permissions()

    def get_queryset(self):
        return Issue.objects.filter(
            project=self.kwargs['project_pk'],
            project__contributors__user=self.request.user
        ).all()


class CommentViewSet(ModelViewSet):
    serializer_class = CommentSerializer

    def get_permissions(self):
        self.permission_classes = [
            IsAuthenticated,
            IsProjectContributorPermission(self.kwargs['project_pk'])
        ]

        if self.action in ('destroy', 'update', 'partial_update'):
            self.permission_classes.append(IsCommentAuthorPermission(self.kwargs['pk']))

        return super(self.__class__, self).get_permissions()

    def get_queryset(self):
        return Comment.objects.filter(
            issue__project__contributors__user=self.request.user,
            issue__project=self.kwargs['project_pk'],
            issue=self.kwargs['issue_pk']
        ).all()
