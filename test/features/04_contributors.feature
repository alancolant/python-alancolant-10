Feature: API - Contributors

  Background:
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_USERNAME}" with password "{env:API_PASSWORD}"
    #Create second user
    When do "POST" request to "/api/signup"
      | Field      | Value               |
      | username   | testtest            |
      | password   | testestester        |
      | password2  | testestester        |
      | email      | tester2@example.com |
      | first_name | Tester              |
      | last_name  | Example             |
    Then add JSON value "id" in session as "user2_id"
     #Create project
    When do "POST" request to "/api/projects/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |
    Then response code is "201"
    And add JSON value "id" in session as "project_id"

  Scenario: Can manage issues

    #Create
    When do "POST" request to "/api/projects/{session:project_id}/users/"
      | Field   | Value              |
      | user_id | {session:user2_id} |
    Then response code is "201"
    And response has JSON structure
      | Field   | Value              |
      | user.id | {session:user2_id} |
      | role    | contributor        |
    And add JSON value "id" in session as "contributor_id"

    #Get list
    When do "GET" request to "/api/projects/{session:project_id}/users"
    Then response has JSON structure
      | Field     | Value              |
      | 1.user.id | {session:user2_id} |
      | 1.role    | contributor        |
    And add JSON value "0.id" in session as "author_id"

    #Update
    When do "PUT" request to "/api/projects/{session:project_id}/users/{session:contributor_id}/"
      | Field   | Value              |
      | user_id | {session:user2_id} |
    Then response code is "200"
    And response has JSON structure
      | Field   | Value              |
      | user.id | {session:user2_id} |

    #Update partially
    When do "PATCH" request to "/api/projects/{session:project_id}/users/{session:contributor_id}/"
      | Field   | Value              |
      | user_id | {session:user2_id} |
    Then response code is "200"
    And response has JSON structure
      | Field   | Value              |
      | user.id | {session:user2_id} |

    #Get
    When do "GET" request to "/api/projects/{session:project_id}/users/{session:contributor_id}"
    Then response code is "200"
    And response has JSON structure
      | Field   | Value              |
      | user.id | {session:user2_id} |
      | id      | N/A                |

    #Delete
    When do "DELETE" request to "/api/projects/{session:project_id}/users/{session:contributor_id}/"
    Then response code is "204"

    #Cannot update author
    When do "PUT" request to "/api/projects/{session:project_id}/users/{session:author_id}/"
      | Field   | Value              |
      | user_id | {session:user2_id} |

    Then response code is "400"
    And response has JSON value "user_id.0" "You cannot update author"