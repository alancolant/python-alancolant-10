Feature: API - Projects - Errors

  Background:
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "otherUser" with password "randomPasswordToOtherUser"

    #Create new project project
    When do "POST" request to "/api/projects/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |

    Then response code is "201"
    And add JSON value "id" in session as "project_id"

    Given ensure bearer_token is set for "{env:API_USERNAME}" with password "{env:API_PASSWORD}"

  Scenario: User cannot view other project
    When do "GET" request to "/api/projects/{session:project_id}"
    Then response code is "403"

  Scenario: User cannot update other project
    When do "PUT" request to "/api/projects/{session:project_id}/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |
    Then response code is "403"

  Scenario: User cannot partial update other project
    When do "PATCH" request to "/api/projects/{session:project_id}/"
      | Field | Value        |
      | title | Simple title |
    Then response code is "403"

  Scenario: User cannot delete other project
    When do "DELETE" request to "/api/projects/{session:project_id}/"
    Then response code is "403"