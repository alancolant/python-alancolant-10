from behave import *


@given('ensure bearer_token is set for "{user}" with password "{password}"')
def step_impl(context, user, password):
    # if context.variables_manager.exists('bearer_token'):
    #     return

    context.execute_steps(
        f"""
            When do "POST" request to "/api/login"
              | Field    | Value              |
              | username | {user} |
              | password | {password} |
            """
    )

    if context.response.status_code == 401:
        context.execute_steps(
            f"""
            When do "POST" request to "/api/signup"
              | Field      | Value              |
              | username   | {user} |
              | password   | {password} |
              | password2  | {password} |
              | email      | tester{user}@example.com |
              | first_name | Tester             |
              | last_name  | Example            |
            Then response code is "201"
            When do "POST" request to "/api/login"
              | Field    | Value  |
              | username | {user} |
              | password | {password} |
            Then response code is "200"
            """
        )

    context.execute_steps(
        f'''
        Then add JSON value "access" in session as "bearer_token"
        Given set bearer token "{{session:bearer_token}}" 
        '''
    )
