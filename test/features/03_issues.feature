Feature: API - Issues

  Background:
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_USERNAME}" with password "{env:API_PASSWORD}"
      #Create project
    When do "POST" request to "/api/projects/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |
    Then response code is "201"
    And add JSON value "id" in session as "project_id"

  Scenario: Can manage issues
    #Create issue
    When do "POST" request to "/api/projects/{session:project_id}/issues/"
      | Field    | Value        |
      | title    | Simple title |
      | desc     | Description  |
      | tag      | bug          |
      | priority | low          |
      | status   | todo         |
    Then response code is "201"
    And response has JSON structure
      | Field    | Value        |
      | title    | Simple title |
      | desc     | Description  |
      | tag      | bug          |
      | priority | low          |
      | status   | todo         |
      | id       | N/A          |
    And add JSON value "id" in session as "issue_id"

    #Get issues list
    When do "GET" request to "/api/projects/{session:project_id}/issues"
    Then response has JSON structure
      | Field      | Value        |
      | 0.title    | Simple title |
      | 0.desc     | Description  |
      | 0.tag      | bug          |
      | 0.priority | low          |
      | 0.status   | todo         |

    #Update issue
    When do "PUT" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/"
      | Field    | Value        |
      | title    | Simple title |
      | desc     | Description  |
      | tag      | bug          |
      | priority | low          |
      | status   | wip          |
    Then response code is "200"
    And response has JSON structure
      | Field  | Value |
      | status | wip   |

    #Update partially issue
    When do "PATCH" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/"
      | Field | Value                |
      | title | Simple title patched |
    Then response code is "200"
    And response has JSON structure
      | Field | Value                |
      | title | Simple title patched |

    #Get issue
    When do "GET" request to "/api/projects/{session:project_id}/issues/{session:issue_id}"
    Then response code is "200"
    And response has JSON structure
      | Field    | Value                |
      | title    | Simple title patched |
      | desc     | Description          |
      | tag      | bug                  |
      | priority | low                  |
      | status   | wip                  |

    #Delete issue
    When do "DELETE" request to "/api/projects/{session:project_id}/issues/{session:issue_id}"
    Then response code is "204"

    #@TODO cannot update/delete issue created by other users issues/comments/projects