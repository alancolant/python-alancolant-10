Feature: API - Projects

  Background:
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_USERNAME}" with password "{env:API_PASSWORD}"

  @depend('create')
  Scenario: Can create project and get returned
    #Action
    When do "POST" request to "/api/projects/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field       | Value               |
      | id          | N/A                 |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |
    And add JSON value "id" in session as "project_id"

  @dependsOnPassed('create')
  Scenario: Can get previously created project
    #Action
    When do "GET" request to "/api/projects/{session:project_id}"

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field       | Value                |
      | id          | {session:project_id} |
      | title       | Simple title         |
      | description | Testing description  |
      | type        | Android              |

  @dependsOnPassed('create')
  Scenario: Can update previously created project
    #Action
    When do "PUT" request to "/api/projects/{session:project_id}/"
      | Field       | Value                       |
      | title       | Simple title changed        |
      | description | Testing description changed |
      | type        | Android                     |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field       | Value                       |
      | id          | {session:project_id}        |
      | title       | Simple title changed        |
      | description | Testing description changed |
      | type        | Android                     |


  @dependsOnPassed('create')
  Scenario: Can update partially previously created project
    #Action
    When do "PATCH" request to "/api/projects/{session:project_id}/"
      | Field | Value                          |
      | title | Simple title changed partially |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field       | Value                          |
      | id          | {session:project_id}           |
      | title       | Simple title changed partially |
      | description | Testing description changed    |
      | type        | Android                        |

  @dependsOnPassed('create')
  Scenario: Can delete previously created project
    When do "DELETE" request to "/api/projects/{session:project_id}/"
    Then response code is "204"