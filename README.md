# Requirements

- Python 3.9.x

# Usage

## With venv

```sh
python3 -m venv /path/to/new/virtual/environment #Create venv
source /path/to/new/virtual/environment/bin/activate 

pip install -r requirements.txt

cd sources
python manage.py runserver
```

## Without venv

```sh
pip install -r requirements.txt
cd sources
python manage.py runserver
```

# Development

## Testing (behave)

### Firstly, you need to create .env in test folder

```env
API_URL=http://127.0.0.1:8000
API_USERNAME=testinguser
API_PASSWORD=RandomPassword
```

```bash
cd test
behave
```

## Interface

* http://127.0.0.1:8000/