from django.urls import path
from rest_framework.routers import flatten
from rest_framework_nested import routers
from rest_framework_simplejwt.views import TokenObtainPairView

from core.views import ProjectViewSet, IssueViewSet, ContributorViewSet, CommentViewSet, RegisterView

router = routers.SimpleRouter()
router.register('projects', ProjectViewSet, basename='projects')

projects_subrouter = routers.NestedSimpleRouter(router, 'projects', lookup='project')
projects_subrouter.register('users', ContributorViewSet, basename='projects_contributors')
projects_subrouter.register('issues', IssueViewSet, basename='projects_issues')

issues_subrouter = routers.NestedSimpleRouter(projects_subrouter, 'issues', lookup='issue')
issues_subrouter.register('comments', CommentViewSet, basename='issues_comments')
# router.register('projects/<int:project_id>/users', UserViewSet, basename='users')
# router.register('projects/<int:project_id>/issues', IssueViewSet, basename='issues')
# router.register('projects/<int:project_id>/issues/<int:issue_id>/comments', CommentViewSet, basename='comments')


urlpatterns = list(flatten([
    [
        path('login', TokenObtainPairView.as_view(), name='token_obtain_pair'),
        path('signup', RegisterView.as_view(), name='register')
    ],
    router.urls,
    projects_subrouter.urls,
    issues_subrouter.urls
]))
