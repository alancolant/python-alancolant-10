Feature: API - Comments

  Background:
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_USERNAME}" with password "{env:API_PASSWORD}"
      #Create project
    When do "POST" request to "/api/projects/"
      | Field       | Value               |
      | title       | Simple title        |
      | description | Testing description |
      | type        | Android             |
    Then response code is "201"
    And add JSON value "id" in session as "project_id"
    #Create issue
    When do "POST" request to "/api/projects/{session:project_id}/issues/"
      | Field    | Value        |
      | title    | Simple title |
      | desc     | Description  |
      | tag      | bug          |
      | priority | low          |
      | status   | todo         |
    Then response code is "201"
    And add JSON value "id" in session as "issue_id"

  Scenario: Can manage comments
    #Create
    When do "POST" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/comments/"
      | Field       | Value       |
      | description | Description |

    Then response code is "201"
    And response has JSON structure
      | Field       | Value       |
      | description | Description |
      | id          | N/A         |
    And add JSON value "id" in session as "comment_id"

    #Get list
    When do "GET" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/comments"
    Then response has JSON structure
      | Field | Value                |
      | 0.id  | {session:comment_id} |

    #Update
    When do "PUT" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/comments/{session:comment_id}/"
      | Field       | Value               |
      | description | Description changed |
    Then response code is "200"
    And response has JSON structure
      | Field       | Value               |
      | description | Description changed |

    #Update partially
    When do "PATCH" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/comments/{session:comment_id}/"
      | Field       | Value                        |
      | description | Description partially update |
    Then response code is "200"
    And response has JSON structure
      | Field       | Value                        |
      | description | Description partially update |

    #Get
    When do "GET" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/comments/{session:comment_id}"
    Then response code is "200"
    And response has JSON structure
      | Field       | Value                        |
      | description | Description partially update |
      | id          | {session:comment_id}         |

    #Delete
    When do "DELETE" request to "/api/projects/{session:project_id}/issues/{session:issue_id}/comments/{session:comment_id}"
    Then response code is "204"