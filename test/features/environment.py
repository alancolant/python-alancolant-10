import os
import shutil
from os.path import abspath

from behave import use_fixture
from behave.model import Scenario
from behave.runner import Context

from features.fixtures import variable_manager, dependencies, skip_tag


def before_all(context):
    use_fixture(variable_manager.before_all, context)
    use_fixture(dependencies.before_all, context)

    context.__ = lambda p: get_sanitized_text(context, p)

    # CLONE DATABASE FROM PROD TO RESTORE LATER
    shutil.copyfile(abspath('../sources/db.sqlite3'), abspath('../sources/db.sqlite3_back'))


def after_all(context):
    # CLONE DATABASE FROM PROD TO RESTORE LATER
    os.remove(abspath('../sources/db.sqlite3'))
    shutil.move(abspath('../sources/db.sqlite3_back'), abspath('../sources/db.sqlite3'))


def before_scenario(context, scenario):
    use_fixture(skip_tag.before_scenario, context, scenario=scenario)
    use_fixture(dependencies.before_scenario, context, scenario=scenario)


def after_scenario(context: Context, scenario: Scenario):
    use_fixture(dependencies.after_scenario, context, scenario=scenario)


def get_sanitized_text(context, text):
    return context.variables_manager.get_replaced_value(text)
