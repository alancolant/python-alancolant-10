import json

import requests
from behave import *


@given('set base_url "{url}"')
def step_impl(context, url):
    context.variables_manager.set('http_api.base_url', context.__(url))


@given('set bearer token "{token}"')
def step_impl(context, token):
    context.variables_manager.set('http_api.bearer_token', context.__(token))


@when('do "{method}" request to "{endpoint}"')
def step_impl(context, method: str, endpoint: str):
    endpoint = context.__(endpoint)
    params = {}
    if context.table is not None:
        for row in context.table:
            field = context.__(row['Field'])
            value = context.__(row['Value'])
            params[field] = value

    url = context.variables_manager.get('http_api.base_url') + endpoint \
        if context.variables_manager.exists('http_api.base_url') \
        else endpoint

    headers = {}
    if context.variables_manager.exists('http_api.bearer_token'):
        headers['Authorization'] = 'Bearer ' + context.variables_manager.get('http_api.bearer_token')

    match (method.upper()):
        case 'POST' | 'PUT' | 'PATCH' | 'DELETE' | 'GET':
            context.response = getattr(requests, method.lower())(
                url=url,
                data=json.dumps(params) if len(params) > 0 else None,
                headers={
                    "Content-Type": 'application/json',
                    "Accept": 'application/json',
                    **headers
                }
            )
        case 'GET':
            context.response = requests.get(
                url=url,
                params=params if len(params) > 0 else None,
                headers=headers
            )
        case _:
            raise Exception(f'Invalid method {method}')


@then('add JSON value "{key}" in session as "{variable_name}"')
def step_impl(context, key, variable_name):
    value = get_dotted_key_value(context.response.json(), key)
    assert value is not None, f'response as no JSON key "{key}"'
    context.variables_manager.set(variable_name, value)


@then('response code is "{code}"')
def step_impl(context, code):
    assert str(context.response.status_code) == code, \
        f'expected "{code}" but received "{context.response.status_code}" ({context.response.text})'


@then('response has JSON value "{key}" "{expected}"')
def step_impl(context, key: str, expected: str):
    key = context.__(key)
    expected = context.__(expected)
    value = get_dotted_key_value(context.response.json(), key)
    assert str(value) == str(expected), f'expected "{expected}" but received "{value}"'


@then('response has JSON key "{key}"')
def step_impl(context, key):
    key = context.__(key)
    value = get_dotted_key_value(context.response.json(), key)
    assert value is not None, f'response as no JSON key "{key}"'


@then('response has JSON structure')
def step_impl(context):
    print(context.response.json())
    print("\n")
    for row in context.table:
        context.execute_steps(
            f'Then response has JSON key "{row["Field"]}"'
            if row['Value'] == 'N/A'
            else f'Then response has JSON value "{row["Field"]}" "{row["Value"]}"'
        )


def get_dotted_key_value(obj: dict | list, key: str):
    key_paths = key.split('.')
    value = obj
    for key_path in key_paths:
        if isinstance(value, list):
            if (len(value) - 1) >= int(key_path):
                value = value[int(key_path)]
            else:
                value = None
                break
        elif key_path in value or {}:
            value = value[key_path]
        else:
            value = None
            break

    return value
